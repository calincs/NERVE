package ca.lincsproject.nssi.broker.message;

import lombok.*;

import java.util.List;
import java.util.Optional;

/**
 * A data class that contains the list of all matches for a given entity
 * returned by a linking service.
 */

@Data
@Builder(builderClassName = "LinkingServiceResultBuilder")
@AllArgsConstructor
@NoArgsConstructor
public class LinkingServiceResult {
    /**
     * The URI of the linking service used.
     */
    private String serviceURI;

    /**
     * The name of the linking service used.
     */
    private String serviceName;

    /**
     * A list of entity matches found by the linking service.
     */
    @Singular
    private List<EntityLinkDetails> matches;

    public static class LinkingServiceResultBuilder {
        public Optional<LinkingServiceResult> buildOptional() {

            // Only construct a result if there were any URIs found for the entity.
            if (matches.isEmpty()) {
                return Optional.empty();
            }

            return Optional.of(this.build());
        }
    }
}
