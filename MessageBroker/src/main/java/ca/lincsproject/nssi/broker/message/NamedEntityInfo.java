package ca.lincsproject.nssi.broker.message;

import lombok.*;

import java.util.List;

@Data
@Builder(builderClassName = "Builder")
@AllArgsConstructor
@NoArgsConstructor
public class NamedEntityInfo {
    /**
     * The entity name.
     */
    private String entity;

    /**
     * The type of entity.
     */
    private EntityClassification classification;

    @Data
    @lombok.Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SelectionWithLemma {
        /**
         * The name of the entity exactly as found in the text.
         */
        private String lemma;

        /**
         * The location of the entity within the text.
         */
        private Selection selection;
    }

    /**
     * A list of occurences of the named entity within the text.
     */
    @Singular
    private List<SelectionWithLemma> selections;
}
