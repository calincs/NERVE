package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class NSSIServiceMessage implements ServiceMessage {
    /**
     * The job ID.
     */
    private Long jobId;

    /**
     * A unique ID attached to the request.
     */
    private String requestId;

    private String messageId;

    /**
     * The project name.
     */
    private String projectName;

    /**
     * A URI for the document to process.
     */
    private String documentURI;

    /**
     * The contents of the document to process.
     */
    private String document;

    /**
     * The format of the document.
     */
    private DocumentMIMEType format;

    /**
     * A list of authorities to use for linking.
     */
    private List<String> authorities;

    /**
     * A map of settings that control various aspects of the processing.
     */
    private Map<String, String> context;

    /**
     * The URI for the processing results.
     */
    private String resultsUri;

    /**
     * The processing pipeline to use.
     */
    private List<String> pipeline;
}
