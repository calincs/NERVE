package ca.lincsproject.nssi.broker.service;

import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.broker.message.CancelMessage;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.ProgressMessage;
import ca.lincsproject.nssi.broker.message.ResultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class RMQUtilsService {
    private final RabbitTemplate rabbitTemplate;
    private final Logger LOGGER = LoggerFactory.getLogger(RMQUtilsService.class);

    @Autowired
    RMQUtilsService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendToQueue(String queueName, Object message) {
        rabbitTemplate.convertAndSend(queueName, message);
    }

    public void sendToQueue(String exchange, String queueName, Object message) {
        rabbitTemplate.convertAndSend(exchange, queueName, message);
    }

    public <U> void sendToNextStep(Class<U> serviceType, NSSIServiceMessage message) {
        List<String> pipeline = message.getPipeline();

        if (Objects.isNull(pipeline) || pipeline.isEmpty()) {
            // Inform NSSI that processing is complete.
            sendProgressMessage(ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .taskMessageId(message.getMessageId())
                    .serviceName(serviceType.getName())
                    .completed(true)
                    .build());
        } else {
            String next = pipeline.get(0);
            List<String> remainingAfterNext = pipeline.subList(1, pipeline.size());
            message.setPipeline(remainingAfterNext);
            message.setMessageId(UUID.randomUUID().toString());

            // Inform NSSI that the message is being enqueued.
            sendProgressMessage(ProgressMessage.builder()
                    .jobId(message.getJobId())
                    .requestId(message.getRequestId())
                    .taskMessageId(message.getMessageId())
                    .serviceName(serviceType.getName())
                    .queued(true)
                    .build());

            rabbitTemplate.convertAndSend(RMQConfiguration.APP_MESSAGES_EXCHANGE, next, message);
        }
    }

    public void sendResultMessage(ResultMessage message) {
        rabbitTemplate.convertAndSend(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.RESULT_QUEUE, message);
    }

    public void sendCancelMessage(String queue, CancelMessage message) {
        rabbitTemplate.convertAndSend(RMQConfiguration.APP_MESSAGES_EXCHANGE, queue, message);
    }

    private void sendProgressMessage(ProgressMessage message) {
        rabbitTemplate.convertAndSend(RMQConfiguration.PROGRESS_EXCHANGE, RMQConfiguration.PROGRESS_QUEUE, message);
    }
}
