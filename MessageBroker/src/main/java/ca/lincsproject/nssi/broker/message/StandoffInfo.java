package ca.lincsproject.nssi.broker.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder(builderClassName = "Builder")
@AllArgsConstructor
@NoArgsConstructor
public class StandoffInfo {
    /**
     * The standoff annotations (i.e., XML elements) contained within
     * the document.
     */
    private List<Annotation> annotations;

    /**
     * The plaintext version of the document.
     */
    private String plain;

    @Data
    @lombok.Builder(builderClassName = "Builder")
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Annotation {
        /**
         * The character start position of the text contained in the annotation.
         */
        private Integer start;

        /**
         * The character end position of the text contained in the annotation.
         */
        private Integer end;

        /**
         * The label of the annotation.
         */
        private String label;

        /**
         * A map of attributes for the annotation.
         */
        private Map<String, String> attributes;

        /**
         * The node depth of the annotation.
         */
        private Integer depth;
    }
}
