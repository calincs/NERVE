package ca.lincsproject.nssi.broker.service;

public interface ThrowingVoidConsumer<T, E extends Throwable> extends ThrowingBiConsumer<T, Void, E> {
}
