package ca.lincsproject.nssi.s3_api.service;

import ca.lincsproject.nssi.s3_api.repository.LincsS3Repository;
import ca.lincsproject.nssi.s3_api.S3APIModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.List;

@Profile(S3APIModuleConstants.PROFILE_NAME)
@Service
public class LincsS3Service {
    private final LincsS3Repository s3Repository;

    @Autowired
    LincsS3Service(LincsS3Repository s3Repository) {
        this.s3Repository = s3Repository;
    }

    public byte[] getObject(String bucket, String object, String requestId) {
        return s3Repository.getObject(bucket, object, requestId);
    }

    public List<Pair<String, byte[]>> getAllObjects(String bucket, String objectPrefix, String requestId) {
        return s3Repository.getAllObjects(bucket, objectPrefix, requestId);
    }

    public List<String> getAllObjectNames(String bucket, String objectPrefix, String requestId) {
        return s3Repository.getObjectNamesFromDir(bucket, objectPrefix, requestId);
    }

    public void putObject(String bucket, String object, byte[] contents, String requestId) {
        s3Repository.putObject(bucket, object, contents, requestId);
    }

    public void deleteObject(String bucket, String object, String requestId) {
        s3Repository.deleteObject(bucket, object, requestId);
    }

    public void deleteAllObjects(String bucket, String object, String requestId) {
        s3Repository.deleteAllObjects(bucket, object, requestId);
    }
}
