package ca.lincsproject.nssi.s3_api;

public class S3APIModuleConstants {
    public static final String PROFILE_NAME = "s3api";
    public static final String SERVICE_NAME = "S3 API Service";
}
