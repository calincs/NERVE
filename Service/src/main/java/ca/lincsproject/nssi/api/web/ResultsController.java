package ca.lincsproject.nssi.api.web;

import ca.lincsproject.nssi.api.infrastructure.config.NSSIProcessingConfigProperties;
import ca.lincsproject.nssi.api.infrastructure.config.WorkflowResultsType;
import ca.lincsproject.nssi.api.service.DatabaseResultsService;
import ca.lincsproject.nssi.api.service.ElucidateResultsService;
import ca.lincsproject.nssi.api.service.ProcessingResult;
import ca.lincsproject.nssi.api.service.S3ResultsService;
import ca.lincsproject.nssi.apiexception.infrastructure.exception.JobCancelledOrFailedException;
import ca.lincsproject.nssi.apiexception.infrastructure.exception.JobNotReadyException;
import ca.lincsproject.nssi.apiexception.infrastructure.exception.ResourceNotFoundException;
import ca.lincsproject.nssi.api.APIModuleConstants;
import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.job.model.JobStatus;
import ca.lincsproject.nssi.job.service.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * This controller provides the NSSI API endpoints related to job results.
 */

@Profile(APIModuleConstants.PROFILE_NAME)
@RestController
public class ResultsController {
    private final JobService jobService;
    private final S3ResultsService s3ResultsService;
    private final ElucidateResultsService elucidateResultsService;
    private final DatabaseResultsService databaseResultsService;
    private final NSSIProcessingConfigProperties processingConfigProperties;
    private final Logger LOGGER = LoggerFactory.getLogger(ResultsController.class);

    @Autowired
    ResultsController(JobService jobService,
                      S3ResultsService s3ResultsService,
                      ElucidateResultsService elucidateResultsService,
                      DatabaseResultsService databaseResultsService,
                      NSSIProcessingConfigProperties processingConfigProperties)
    {
        this.jobService = jobService;
        this.s3ResultsService = s3ResultsService;
        this.elucidateResultsService = elucidateResultsService;
        this.databaseResultsService = databaseResultsService;
        this.processingConfigProperties = processingConfigProperties;
    }

    /**
     * Returns the results for the given job.
     * 
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param workflow the workflow under which the given job was processed
     * @param jobId the job ID
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @GetMapping(value = "/api/results/{workflow}/{jobId}")
    public ResponseEntity<? extends ProcessingResult> getResults(Principal principal,
                                                                 @PathVariable("workflow") String workflow,
                                                                 @PathVariable("jobId") Long jobId,
                                                                 @RequestHeader("Authorization") String authToken,
                                                                 @RequestHeader("requestId") String requestId)
            throws JobNotReadyException, ResourceNotFoundException
    {
        var workflowConfig = processingConfigProperties.getWorkflows().get(workflow);

        JobDTO job = jobService.getJob(jobId).orElseThrow(ResourceNotFoundException::new);

        if (job.getStatus().equals(JobStatus.CANCELLED) || job.getStatus().equals(JobStatus.FAILED)) {
            throw new JobCancelledOrFailedException(jobId);
        }

        if (!job.getStatus().equals(JobStatus.READY)) {
            throw new JobNotReadyException(jobId);
        }

        LOGGER.error(workflowConfig.getResultsType().toString());
        if (workflowConfig.getResultsType().equals(WorkflowResultsType.S3)) {
            return ResponseEntity.ok(s3ResultsService.getResults(job, requestId));
        } else if (workflowConfig.getResultsType().equals(WorkflowResultsType.ELUCIDATE)) {
            return ResponseEntity.ok(elucidateResultsService.getResults(job));
        } else if (workflowConfig.getResultsType().equals(WorkflowResultsType.DATABASE)) {
            return ResponseEntity.ok(databaseResultsService.getResults(job));
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Deletes the results for the specified job.
     *
     * @param principal automatically set by Spring, this object represents the authenticated user
     * @param workflow the workflow under which the given job was processed
     * @param jobId the job ID
     * @param authToken the auth token for the request
     * @param requestId the request ID, assigned by the NSSI Gateway
     */
    @DeleteMapping(value = "/api/results/{workflow}/{jobId}")
    public ResponseEntity deleteResults(Principal principal,
                                        @PathVariable("workflow") String workflow,
                                        @PathVariable("jobId") Long jobId,
                                        @RequestHeader("Authorization") String authToken,
                                        @RequestHeader("requestId") String requestId)
            throws JobNotReadyException, ResourceNotFoundException
    {
        LOGGER.info("Deleting Job Results",  
                   kv("service", APIModuleConstants.SERVICE_NAME),
                   kv("jobId", jobId),
                   kv("user", principal.getName()),
                   kv("workflow", workflow),
                   kv("requestId", requestId));

        var job = jobService.getJob(jobId).orElseThrow(ResourceNotFoundException::new);

        if (!job.getStatus().equals(JobStatus.READY)) {
            throw new JobNotReadyException(jobId);
        }

        var workflowConfig = processingConfigProperties.getWorkflows().get(workflow);

        if (workflowConfig.getResultsType().equals(WorkflowResultsType.S3)) {
            s3ResultsService.deleteResults(job, requestId);
        } else if (workflowConfig.getResultsType().equals(WorkflowResultsType.ELUCIDATE)) {
            elucidateResultsService.deleteResults(job);
        } else if (workflowConfig.getResultsType().equals(WorkflowResultsType.DATABASE)) {
            databaseResultsService.deleteResults(job);
        }

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
