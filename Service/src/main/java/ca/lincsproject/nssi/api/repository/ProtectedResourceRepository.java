package ca.lincsproject.nssi.api.repository;

import ca.lincsproject.nssi.api.APIModuleConstants;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.resource.ProtectedResource;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Profile(APIModuleConstants.PROFILE_NAME)
@Repository
public class ProtectedResourceRepository {
    private final AuthzClient authzClient;

    private final Logger LOGGER = LoggerFactory.getLogger(ProtectedResourceRepository.class);

    @Autowired
    ProtectedResourceRepository(AuthzClient authzClient) {
        this.authzClient = authzClient;
    }

    public String createProtectedResource(String resourceName,
                                          String resourceDisplayName,
                                          String resourceType, // FIXME should be enum
                                          String ownerId,
                                          String uri)
    {
        ProtectedResource resourceClient = authzClient.protection().resource();

        ResourceRepresentation newResource = new ResourceRepresentation();
        newResource.setName(resourceName);
        newResource.setDisplayName(resourceDisplayName);
        newResource.setType(resourceType);
        newResource.setOwner(ownerId);
        newResource.setOwnerManagedAccess(true);
        newResource.addScope("view");
        newResource.addScope("delete");

        // The resource type is added as a scope because the Keycloak admin API
        // can retrieve all resources of a particular scope that a user has access
        // to, but _cannot_ (currently) do the same for resources of a particular
        // type. Thus, repeating the type as a scope allows us to show a user, e.g.,
        // a list of all the projects they're authorized to work on.
        newResource.addScope(resourceType);

        newResource.setUris(Set.of(uri));

        ResourceRepresentation response = resourceClient.create(newResource);

        return response.getId();
    }
}
