package ca.lincsproject.nssi.api.infrastructure.config;

public enum WorkflowResultsType {
    ELUCIDATE,
    S3,
    DATABASE
}
