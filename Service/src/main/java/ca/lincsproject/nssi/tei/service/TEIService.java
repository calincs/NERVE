package ca.lincsproject.nssi.tei.service;

import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.broker.message.Selection;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.tei.TEIModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Profile(TEIModuleConstants.PROFILE_NAME)
@Service
public class TEIService {
    private NSSIServiceUtils<String, NamedEntityInfo> nssiServiceUtils;
    private final Logger logger = LoggerFactory.getLogger(TEIService.class);

    @Autowired
    TEIService(@Value("${nssi.processing.services.tei.fullName}") String fullName,
               NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                String.class,
                NamedEntityInfo.class,
                fullName
        );
    }

    @RabbitListener(queues = "teiQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, String document) throws Throwable {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        String cleanedDocument = document.replaceAll("<\\?.*?\\?>", "");
        Document teiDocument = builder.parse(new InputSource(new StringReader(cleanedDocument)));

        List<NamedEntityInfo> entities = new ArrayList<>();
        entities.addAll(getPersons(teiDocument));
        entities.addAll(getPlaces(teiDocument));
        entities.addAll(getOrganizations(teiDocument));

        entities.stream()
                .filter(namedEntityInfo -> !namedEntityInfo.getEntity().isBlank())
                .forEach(entityInfo -> nssiServiceUtils.sendToNextStep(message, entityInfo));
    }

    private List<NamedEntityInfo> getPersons(Document teiDocument) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String listPersonXPath = "//listPerson/person";
        NodeList nodeList = (NodeList) xpath.compile(listPersonXPath).evaluate(teiDocument, XPathConstants.NODESET);

        String forenameXPath = "persName/forename";
        String surnameXPath = "persName/surname";

        List<NamedEntityInfo> result = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            List<String> names = new ArrayList<>();
            Node personNode = nodeList.item(i);
            logger.error("XPATH: " + getXpathFromNode(personNode));
            Optional.ofNullable((Node) xpath.compile(forenameXPath).evaluate(personNode, XPathConstants.NODE))
                    .ifPresent(forename -> {
                        names.add(forename.getTextContent());
                    });

            Optional.ofNullable((Node) xpath.compile(surnameXPath).evaluate(personNode, XPathConstants.NODE))
                    .ifPresent(surname -> names.add(surname.getTextContent()));

            if (names.isEmpty()
                    && personNode.getChildNodes().getLength() == 1
                    && personNode.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE) {
                names.add(nodeList.item(i).getTextContent());
            }

            var entityName = String.join(" ", names);
            NamedEntityInfo namedEntityInfo = NamedEntityInfo.builder()
                    .entity(entityName)
                    .classification(EntityClassification.PERSON)
                    .selection(NamedEntityInfo.SelectionWithLemma.builder()
                            .lemma(entityName)
                            .selection(Selection.builder()
                                    .xpath(getXpathFromNode(personNode))
                                    .build())
                            .build())
                    .build();

            result.add(namedEntityInfo);
        }

        return result;
    }

    private List<NamedEntityInfo> getPlaces(Document teiDocument) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String listPlaceXPath = "//listPlace/place";
        NodeList nodeList = (NodeList) xpath.compile(listPlaceXPath).evaluate(teiDocument, XPathConstants.NODESET);

        String placeNameXPath = "placeName";

        List<NamedEntityInfo> result = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node placeNode = nodeList.item(i);
            Optional.ofNullable((Node) xpath.compile(placeNameXPath).evaluate(placeNode, XPathConstants.NODE))
                    .ifPresent(placeName -> {
                        String entityName = placeName.getTextContent();
                        NamedEntityInfo namedEntityInfo = NamedEntityInfo.builder()
                                .entity(entityName)
                                .classification(EntityClassification.LOCATION)
                                .selection(NamedEntityInfo.SelectionWithLemma.builder()
                                        .lemma(entityName)
                                        .selection(Selection.builder()
                                                .xpath(getXpathFromNode(placeNode))
                                                .build())
                                        .build())
                                .build();

                        result.add(namedEntityInfo);
                    });
        }

        return result;
    }

    private List<NamedEntityInfo> getOrganizations(Document teiDocument) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String listOrgXPath = "//listOrg/org";
        NodeList nodeList = (NodeList) xpath.compile(listOrgXPath).evaluate(teiDocument, XPathConstants.NODESET);

        String orgNameXPath = "orgName";

        List<NamedEntityInfo> result = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node orgNode = nodeList.item(i);
            Optional.ofNullable((Node) xpath.compile(orgNameXPath).evaluate(orgNode, XPathConstants.NODE))
                    .ifPresent(placeName -> {
                        String entityName = placeName.getTextContent();
                        NamedEntityInfo namedEntityInfo = NamedEntityInfo.builder()
                                .entity(entityName)
                                .classification(EntityClassification.ORGANIZATION)
                                .selection(NamedEntityInfo.SelectionWithLemma.builder()
                                        .lemma(entityName)
                                        .selection(Selection.builder()
                                                .xpath(getXpathFromNode(orgNode))
                                                .build())
                                        .build())
                                .build();

                        result.add(namedEntityInfo);
                    });
        }

        return result;
    }

    private String getXpathFromNode(Node node) {
        List<String> xpathElements = new ArrayList<>();
        xpathElements.add(node.getNodeName() + getSiblingIndexStringFromNode(node));

        while (Objects.nonNull(node.getParentNode())
                && node.getParentNode().getNodeType() == Node.ELEMENT_NODE) {
            node = node.getParentNode();
            xpathElements.add(node.getNodeName() + getSiblingIndexStringFromNode(node));
        }

        Collections.reverse(xpathElements);
        return "/" + String.join("/", xpathElements);
    }

    private String getSiblingIndexStringFromNode(Node node) {
        int index = 1;
        Node sibling = node.getPreviousSibling();
        while (Objects.nonNull(sibling)) {
            if (sibling.getNodeType() == Node.ELEMENT_NODE
                    && sibling.getNodeName().equals(node.getNodeName())) {
                index++;
            }
            sibling = sibling.getPreviousSibling();
        }

        if (index == 1 && Objects.nonNull(node.getParentNode())) {
            Node parent = node.getParentNode();
            int nodeCount = 0;
            for (int j = 0; j < parent.getChildNodes().getLength(); j++) {
                if (parent.getChildNodes().item(j).getNodeName().equals(node.getNodeName())) {
                    nodeCount++;
                }
            }

            if (nodeCount > 1) {
                return "[1]";
            } else {
                return "";
            }
        }

        return "[" + index + "]";
    }
}
