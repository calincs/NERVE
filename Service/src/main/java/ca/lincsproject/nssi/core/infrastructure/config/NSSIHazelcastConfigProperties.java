package ca.lincsproject.nssi.core.infrastructure.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "nssi.hazelcast")
public class NSSIHazelcastConfigProperties {
    private String serviceDNS;
    private int maxCacheSize;
    private List<CacheConfig> caches;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CacheConfig {
        private String name;
        private int backup;
        private int ttlSeconds;
    }
}
