package ca.lincsproject.nssi.standoff.model;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class Annotation {
    /**
     * The annotation index.
     */
    private Integer index;

    /**
     * The character start position of the text contained in the annotation.
     */
    private Integer start;

    /**
     * The character end position of the text contained in the annotation.
     */
    private Integer end;

    /**
     * The label of the annotation.
     */
    private String label;

    /**
     * A map of attributes for the annotation.
     */
    private Map<String, String> attributes;

    /**
     * The node depth of the annotation.
     */
    private Integer depth;

    /**
     * Checks whether {@code this} contains {@code other}.
     *
     * @param other another annotation
     * @return true if {@code this} contains {@code other}; false otherwise
     */
    public boolean contains(Annotation other) {
        return start <= other.getStart() && end >= other.getEnd();
    }

    /**
     * Checks whether {@code this} is contained within {@code other}.
     *
     * @param other another annotation
     * @return true if {@code other} contains {@this}; false otherwise
     */
    public boolean containedWithin(Annotation other) {
        return other.getStart() <= start && other.getEnd() >= end;
    }

    /**
     * Checks whether {@code this} precedes {@code other}.
     *
     * @param other another annotation
     * @return true if {@code this} precedes {@code other}; false otherwise
     */
    public boolean precedes(Annotation other) {
        return start <= other.getStart() && end <= other.getStart();
    }

    /**
     * Checks whether {@code this} follows {@code other}.
     *
     * @param other another annotation
     * @return true if {@code this} follows {@code other}; false otherwise
     */
    public boolean follows(Annotation other) {
        return start >= other.getEnd() && end >= other.getEnd();
    }
}
