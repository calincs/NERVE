package ca.lincsproject.nssi.monitor.service;

import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.monitor.MonitorModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(MonitorModuleConstants.PROFILE_NAME)
@Service
public class DLQListenerService {

    private final RabbitTemplate rabbitTemplate;
    private final RMQUtilsService rmqUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(DLQListenerService.class);

    @Autowired
    DLQListenerService(RabbitTemplate rabbitTemplate,
                       RMQUtilsService rmqUtils) {
        this.rabbitTemplate = rabbitTemplate;
        this.rmqUtils = rmqUtils;
    }

    @RabbitListener(queues = RMQConfiguration.DLQ)
    public void listen(Message message) {
        MessageProperties props = message.getMessageProperties();
        String originalExchange = props.getHeader("x-original-exchange");
        String originalRoutingKey = props.getHeader("x-original-routing-key");

        LOGGER.info("Retrying Message Off DLQ",
                   kv("exchange", originalExchange),
                   kv("routingKey", originalRoutingKey));

        rabbitTemplate.convertAndSend(originalExchange, originalRoutingKey, message);
    }
}
