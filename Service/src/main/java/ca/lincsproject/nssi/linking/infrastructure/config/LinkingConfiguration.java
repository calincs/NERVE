package ca.lincsproject.nssi.linking.infrastructure.config;

import ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.wikidata.wdtk.wikibaseapi.WikibaseDataFetcher;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Configuration
public class LinkingConfiguration {

    @Bean
    public WikibaseDataFetcher wikibaseDataFetcher() {
        return WikibaseDataFetcher.getWikidataDataFetcher();
    }

    @Bean
    public Queue linkingQueue() {
        return QueueBuilder.durable(LinkingModuleConstants.QUEUE_NAME)
                .withArgument(RMQConfiguration.DLQ_KEY, RMQConfiguration.DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding linkingBinding(@Qualifier(RMQConfiguration.APP_MESSAGES_EXCHANGE) DirectExchange exchange) {
        return BindingBuilder.bind(linkingQueue()).to(exchange)
                .with(LinkingModuleConstants.QUEUE_NAME);
    }
}
