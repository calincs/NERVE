package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.broker.message.LinkedEntityInfo;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * An orchestration service that manages the linking services used to
 * find URI matches for tagged entities.
 */

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service
public class LinkingOrchestratorService {

    private final List<AsyncLinkingService> linkingServices;

    private final Logger LOGGER = LoggerFactory.getLogger(LinkingOrchestratorService.class);

    @Autowired
    LinkingOrchestratorService(AsyncLinkingService wikidataService,
                               AsyncLinkingService viafService,
                               AsyncLinkingService dbpediaService,
                               AsyncLinkingService lgpn2Service,
                               AsyncLinkingService gettyService,
                               AsyncLinkingService geonamesService)
    {
        this.linkingServices = new ArrayList<>();

        installService(wikidataService);
        installService(viafService);
        installService(dbpediaService);
        installService(lgpn2Service);
        installService(gettyService);
        installService(geonamesService);
    }

    /**
     * Installs the given linking service if it has not already been installed.
     *
     * @param service the service to install.
     */
    public void installService(AsyncLinkingService service) {
        if (this.linkingServices.stream()
                .noneMatch(s -> s.getName().equals(service.getName()))) {
            this.linkingServices.add(service);
        }
    }

    /**
     * Uninstalls the given linking service.
     *
     * @param serviceName the name of the service to uninstall.
     */
    public void uninstallService(LinkingServiceName serviceName) {
        this.linkingServices.removeIf(s -> s.getName().equals(serviceName));
    }

    /**
     * Returns a list of the URIs matched for the given entity, from every
     * installed linking service. See {@link EntityLinks} for a description
     * of the result.
     *
     * @param info a NamedEntityMessage containing details for the entity to link
     */

    public LinkedEntityInfo getLinksForEntity(NamedEntityInfo info,
                                              List<String> authorities)
    {
        List<LinkingServiceName> servicesToUse = getServiceNames(authorities);
        List<AsyncLinkingService> services = servicesToUse.stream()
                .map(requestedService -> linkingServices.stream()
                        .filter(linkingService -> requestedService.equals(linkingService.getName()))
                        .findFirst()
                )
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        List<CompletableFuture<Optional<LinkingServiceResult>>> resultFutures = new ArrayList<>();

        for (AsyncLinkingService service : services) {
            resultFutures.add(service.getLinks(info.getEntity(), info.getClassification().name()));
        }

        CompletableFuture.allOf(resultFutures.toArray(CompletableFuture[]::new)).join();

        List<LinkingServiceResult> results = resultFutures.stream()
                .map(future -> {
                    try {
                        return future.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        return Optional.<LinkingServiceResult>empty();
                    }
                })
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        LinkedEntityInfo leInfo = LinkedEntityInfo.builder()
                .namedEntityInfo(info)
                .links(results)
                .build();

        return leInfo;
    }

    private List<LinkingServiceName> getServiceNames(List<String> names) {
        if (Objects.isNull(names)) {
            return Collections.emptyList();
        }

        return names.stream()
                .map(name -> {
                    try {
                        return LinkingServiceName.valueOf(name.toUpperCase());
                    } catch (IllegalArgumentException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
