package ca.lincsproject.nssi.linking.service;

import ca.lincsproject.nssi.linking.repository.GettyRepository;
import ca.lincsproject.nssi.linking.repository.GettyResponse;
import ca.lincsproject.nssi.linking.repository.LinkingServiceName;
import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.EntityLinkDetails;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.linking.LinkingModuleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Profile(LinkingModuleConstants.PROFILE_NAME)
@Service(value = "gettyService")
public class GettyService implements AsyncLinkingService {

    private final GettyRepository repository;

    @Autowired
    public GettyService(GettyRepository repository) {
        this.repository = repository;
    }

    @Override
    public LinkingServiceName getName() {
        return LinkingServiceName.GETTY;
    }

    @Async
    @Override
    public CompletableFuture<Optional<LinkingServiceResult>> getLinks(String entity, String type)
    {
        if (type.equals(EntityClassification.LOCATION.name())
                || type.equals(EntityClassification.TITLE.name())) {
            return CompletableFuture.completedFuture(Optional.empty());
        }

        GettyResponse response = repository.getResult(entity);

        List<EntityLinkDetails> matches = response.getResults().getBindings().stream()
                .map(result -> EntityLinkDetails.builder()
                        .uri(result.getSubject().getValue())
                        .heading(result.getTerm().getValue())
                        .description(result.getDescription().getValue())
                        .build()
                ).collect(Collectors.toList());

        return CompletableFuture.completedFuture(
                LinkingServiceResult.builder()
                        .serviceName(this.getName().getServiceName())
                        .serviceURI(this.getName().getUri())
                        .matches(matches)
                        .buildOptional()
        );
    }
}
