package ca.lincsproject.nssi.stanfordner.service;

import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.RMQUtilsService;
import ca.lincsproject.nssi.stanfordner.StanfordNERModuleConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class PlaintextProcessor {
    private final XMLProcessorUtils xmlProcessorUtils;
    private final RMQUtilsService rmqUtils;
    private final ObjectMapper objectMapper;

    private final Logger LOGGER = LoggerFactory.getLogger(PlaintextProcessor.class);

    @Autowired
    PlaintextProcessor(XMLProcessorUtils xmlProcessorUtils,
                       RMQUtilsService rmqUtils,
                       ObjectMapper objectMapper)
    {
        this.xmlProcessorUtils = xmlProcessorUtils;
        this.rmqUtils = rmqUtils;
        this.objectMapper = objectMapper;
    }

    public void handleMessage(NSSIServiceMessage message,
                              String document,
                              NERPipeline classifier)
    {
        var classifierResults = classifier.run(document, 0);
        if (classifierResults.size() == 0) {
            LOGGER.info("No entities found.");
        } else {
            var namedEntities = xmlProcessorUtils.getNamedEntityInfo(classifierResults);
            var resultMessages = xmlProcessorUtils.convertNamedEntityResults(namedEntities, message);

            resultMessages.forEach(m -> rmqUtils.sendToNextStep(StanfordNERService.class, m));
        }
    }
}
