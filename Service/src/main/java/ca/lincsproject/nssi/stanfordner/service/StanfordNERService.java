package ca.lincsproject.nssi.stanfordner.service;

import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import ca.lincsproject.nssi.stanfordner.StanfordNERModuleConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile(StanfordNERModuleConstants.PROFILE_NAME)
@Service
public class StanfordNERService {
    private final XMLProcessor xmlProcessor;
    private final PlaintextProcessor plaintextProcessor;
    private final DefaultNERPipeline defaultPipeline;
    private final CorefNERPipeline corefPipeline;
    private final NSSIServiceUtils<String, NamedEntityInfo> nssiServiceUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(StanfordNERService.class);

    @Autowired
    public StanfordNERService(@Value("${nssi.processing.services.stanfordner.fullName}") String fullName,
                              DefaultNERPipeline defaultPipeline,
                              CorefNERPipeline corefPipeline,
                              XMLProcessor xmlProcessor,
                              PlaintextProcessor plaintextProcessor,
                              NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.defaultPipeline = defaultPipeline;
        this.corefPipeline = corefPipeline;
        this.xmlProcessor = xmlProcessor;
        this.plaintextProcessor = plaintextProcessor;
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                String.class,
                NamedEntityInfo.class,
                fullName
        );
    }

    @RabbitListener(queues = "stanfordnerQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, String document) {
        switch (message.getFormat()) {
            case TEI_XML:
                xmlProcessor.handleMessage(message, document,"teiHeader", corefPipeline);
                break;
            case ORLANDO_XML:
                xmlProcessor.handleMessage(message,  document,"ORLANDOHEADER", corefPipeline);
                break;
            case MODS:
            case EAD:
            case PDF:
            case HTML:
            case PLAIN_TEXT:
                plaintextProcessor.handleMessage(message, document, defaultPipeline);
            default:
                // TODO
        }
    }
}
