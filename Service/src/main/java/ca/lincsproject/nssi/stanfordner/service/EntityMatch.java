package ca.lincsproject.nssi.stanfordner.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EntityMatch {
    private String entityMention;
    private String entityXpath;
    private Integer startPosition;
    private Integer endPosition;
}
