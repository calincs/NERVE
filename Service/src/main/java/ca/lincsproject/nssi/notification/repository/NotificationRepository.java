package ca.lincsproject.nssi.notification.repository;

import ca.lincsproject.nssi.notification.NotificationModuleConstants;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Profile(NotificationModuleConstants.PROFILE_NAME)
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    List<Notification> findByClientId(String clientId);
    Long deleteByClientId(String clientId);
    
}
