package ca.lincsproject.nssi.notification.service;

import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.job.service.JobService;
import ca.lincsproject.nssi.notification.repository.Notification;
import ca.lincsproject.nssi.notification.repository.NotificationRepository;
import ca.lincsproject.nssi.notification.NotificationModuleConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(NotificationModuleConstants.PROFILE_NAME)
@Transactional
@Service
public class NotificationService {
    private final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);
    private final ObjectMapper objectMapper;

    private final HashMap<String, SseEmitter> emitters;
    private final NotificationRepository notifRepo;
    private final JobService jobService;

    @Autowired
    public NotificationService(ObjectMapper objectMapper,
                               NotificationRepository notifRepo,
                               JobService jobService) 
    {
        this.objectMapper = objectMapper;
        this.notifRepo = notifRepo;
        this.jobService = jobService;

        emitters = new HashMap<>();
    }

    public void addClient(String clientId, SseEmitter emitter, String requestId) {
        emitter.onTimeout(() -> emitters.remove(clientId));
        emitter.onCompletion(() -> emitters.remove(clientId));
        emitters.put(clientId, emitter);

        for (Notification notif : notifRepo.findByClientId(clientId)) {
            try {
                emitters.get(notif.getClientId())
                        .send(SseEmitter.event()
                                        .data(notif.getBody())
                                        .name(notif.getJobId()));
            } catch (IOException e) {
                LOGGER.error("FAILED To Add Notification Client",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", NotificationModuleConstants.SERVICE_NAME),
                            kv("clientId", clientId),
                            kv("requestId", requestId));
                emitters.get(notif.getClientId()).complete();
            }
        }
        notifRepo.deleteByClientId(clientId);
    }

    public void sendFinishedMessage(Long jobId) {
        JobDTO job = jobService.getJob(jobId).orElse(null);
        if (job == null) {
            LOGGER.error("FAILED To Retrieve JobDTO",  
                        kv("service", NotificationModuleConstants.SERVICE_NAME),
                        kv("jobId", jobId),
                        kv("requestId", job.getRequestId()));
            return;
        }

        if (job.getClientId() != null && emitters.containsKey(job.getClientId())) {
            try {
                emitters.get(job.getClientId())
                        .send(SseEmitter.event()
                                        .data(objectMapper.writeValueAsString(job))
                                        .name(job.getId().toString()));
            } catch (IOException e) {
                LOGGER.error("FAILED To Send Notification",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", NotificationModuleConstants.SERVICE_NAME),
                            kv("jobId", jobId),
                            kv("requestId", job.getRequestId()));
                emitters.get(job.getClientId()).complete();
            }
        } else if (job.getClientId() != null) {
            try {
                Notification notif = Notification.builder()
                                                 .clientId(job.getClientId())
                                                 .jobId(job.getId().toString())
                                                 .body(objectMapper.writeValueAsString(job))
                                                 .time(LocalDateTime.now())
                                                 .build();

                notifRepo.save(notif);
            } catch (IOException e) {
                LOGGER.error("FAILED To Save Notification",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", NotificationModuleConstants.SERVICE_NAME),
                            kv("jobId", jobId),
                            kv("requestId", job.getRequestId()));
            }
        }
    }

    @Scheduled(fixedDelayString = "${nssi.notification.refresh_period_in_ms}")
    public void refreshEmitters() {
        for (SseEmitter emitter : emitters.values()) {
            try {
                emitter.send(SseEmitter.event().data("{\"connection\":\"" + LocalDateTime.now() + "\"}").name("serverEvent"));
            } catch (IOException e) {
                LOGGER.error("FAILED To Refresh Emitter Connection",  
                            kv("stackTrace", e.getStackTrace()), 
                            kv("service", NotificationModuleConstants.SERVICE_NAME));
                emitter.complete();
            }
        }
    }

}
