package ca.lincsproject.nssi.project.service;

import ca.lincsproject.nssi.job.model.ProjectDTO;
import ca.lincsproject.nssi.job.model.ProjectResourceDTO;
import ca.lincsproject.nssi.job.repository.Project;
import ca.lincsproject.nssi.job.repository.ProjectRepository;
import ca.lincsproject.nssi.job.repository.ProjectResource;
import ca.lincsproject.nssi.job.repository.ProjectResourceRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectResourceRepository projectResourceRepository;

    @Autowired
    ProjectService(ProjectRepository projectRepository,
                   ProjectResourceRepository projectResourceRepository)
    {
        this.projectRepository = projectRepository;
        this.projectResourceRepository = projectResourceRepository;
    }

    public List<ProjectDTO> getProjectsList()
    {
        return projectRepository.findByDeletedFalse().stream()
                .map(this::toProjectDTO)
                .collect(Collectors.toList());
    }

    public ProjectDTO newProject(String name)
    {
        Project project = Project.builder()
                .projectName(name)
                .deleted(false)
                .build();

        projectRepository.save(project);
        return toProjectDTO(project);
    }

    public Optional<ProjectDTO> getProject(Long projectId)
    {
        return projectRepository.findById(projectId)
                .map(this::toProjectDTO);
    }

    public void deleteProject(Long projectId)
    {
        projectRepository.findById(projectId)
                .ifPresent(project -> {
                    project.setDeleted(true);
                    projectRepository.save(project);
                });
    }

    public ProjectResourceDTO createProjectResource(Long projectId, String sourceDocumentUri, String resultsUri)
    {
        Project project = projectRepository.findById(projectId).orElseThrow();

        ProjectResource projectResource = ProjectResource.builder()
                .id(UUID.randomUUID())
                .project(project)
                .sourceDocumentUri(sourceDocumentUri)
                .resultsUri(resultsUri)
                .deleted(false)
                .build();

        projectResourceRepository.save(projectResource);
        return toProjectResourceDTO(projectResource);
    }

    public Optional<ProjectResourceDTO> getProjectResource(Long projectId, String resourceId)
    {
        return projectRepository.findById(projectId)
                .map(this::toProjectDTO)
                .map(ProjectDTO::getResources).orElse(Collections.emptyList()).stream()
                .filter(projectResourceDTO -> StringUtils.equals(projectResourceDTO.getId(), resourceId))
                .findFirst();
    }

    public void deleteProjectResource(Long projectId, String resourceId)
    {
        projectResourceRepository.findById(resourceId)
                .filter(projectResource -> Objects.equals(projectResource.getProject().getId(), projectId))
                .ifPresent(projectResource -> {
                    projectResource.setDeleted(true);
                    projectResourceRepository.save(projectResource);
                });
    }

    private ProjectDTO toProjectDTO(Project project) {
        ProjectDTO.Builder projectBuilder = ProjectDTO.builder()
                .id(project.getId())
                .projectName(project.getProjectName());

        Optional.ofNullable(project.getResources()).orElse(Collections.emptyList()).stream()
                .map(this::toProjectResourceDTO)
                .forEach(projectBuilder::resource);

        return projectBuilder.build();
    }

    private ProjectResourceDTO toProjectResourceDTO(ProjectResource projectResource)
    {
        return ProjectResourceDTO.builder()
                .id(projectResource.getId().toString())
                .sourceDocumentUri(projectResource.getSourceDocumentUri())
                .resultsUri(projectResource.getResultsUri())
                .build();
    }
}
