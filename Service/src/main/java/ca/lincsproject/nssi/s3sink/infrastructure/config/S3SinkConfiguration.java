package ca.lincsproject.nssi.s3sink.infrastructure.config;

import ca.lincsproject.nssi.s3sink.S3SinkModuleConstants;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static ca.lincsproject.nssi.broker.infrastructure.config.RMQConfiguration.*;

@Profile(S3SinkModuleConstants.PROFILE_NAME)
@Configuration
public class S3SinkConfiguration {
    @Bean
    public Queue s3SinkQueue(@Value("${nssi.processing.services.s3sink.jobQueue}") String queueName) {
        return QueueBuilder.durable(queueName)
                .withArgument(DLQ_KEY, DLX_APP_MESSAGES)
                .build();
    }

    @Bean
    public Binding s3SinkBinding(@Qualifier(APP_MESSAGES_EXCHANGE) DirectExchange exchange,
                                 @Value("${nssi.processing.services.s3sink.jobQueue}") String queueName) {
        return BindingBuilder.bind(s3SinkQueue(queueName)).to(exchange)
                .with(queueName);
    }
}
