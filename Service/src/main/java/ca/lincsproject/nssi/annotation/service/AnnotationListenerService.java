package ca.lincsproject.nssi.annotation.service;

import ca.lincsproject.nssi.annotation.AnnotationModuleConstants;
import ca.lincsproject.nssi.broker.message.LinkedEntityInfo;
import ca.lincsproject.nssi.broker.message.NSSIServiceMessage;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtils;
import ca.lincsproject.nssi.broker.service.NSSIServiceUtilsFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Profile(AnnotationModuleConstants.PROFILE_NAME)
@RabbitListener(id = "annotation")
@Service
public class AnnotationListenerService {
    private final AnnotationService annotationService;
    private final NSSIServiceUtils<LinkedEntityInfo, Void> nssiServiceUtils;

    private final Logger LOGGER = LoggerFactory.getLogger(AnnotationListenerService.class);

    @Autowired
    AnnotationListenerService(@Value("${nssi.processing.services.annotation.fullName}") String fullName,
                              AnnotationService annotationService,
                              NSSIServiceUtilsFactory nssiServiceUtilsFactory)
    {
        this.annotationService = annotationService;
        this.nssiServiceUtils = nssiServiceUtilsFactory.nssiServiceUtils(
                LinkedEntityInfo.class,
                Void.class,
                fullName
        );
    }

    @RabbitListener(queues = "annotationQueue")
    public void listen(Message message) {
        nssiServiceUtils.processWithRetry(message, this::processMessage);
    }

    private void processMessage(NSSIServiceMessage message, LinkedEntityInfo leInfo) throws Throwable {
        annotationService.createAnnotationsForLinkedEntity(
                message.getJobId(),
                message.getMessageId(),
                message.getDocumentURI(),
                message.getResultsUri(),
                message.getFormat(),
                leInfo,
                message.getRequestId()
        );
    }
}
