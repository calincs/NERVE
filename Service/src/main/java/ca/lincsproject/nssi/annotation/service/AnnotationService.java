package ca.lincsproject.nssi.annotation.service;

import ca.lincsproject.nssi.elucidate_api.model.Annotation;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationBody;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationChoice;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationContainer;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationContext;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationCreator;
import ca.lincsproject.nssi.elucidate_api.model.AnnotationTarget;
import ca.lincsproject.nssi.elucidate_api.model.TextPositionSelector;
import ca.lincsproject.nssi.elucidate_api.model.XpathSelector;
import ca.lincsproject.nssi.elucidate_api.service.AnnotationClient;
import ca.lincsproject.nssi.annotation.AnnotationModuleConstants;
import ca.lincsproject.nssi.broker.message.DocumentMIMEType;
import ca.lincsproject.nssi.broker.message.EntityClassification;
import ca.lincsproject.nssi.broker.message.LinkedEntityInfo;
import ca.lincsproject.nssi.broker.message.LinkingServiceResult;
import ca.lincsproject.nssi.broker.message.NamedEntityInfo;
import ca.lincsproject.nssi.broker.message.Selection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.ANNOTATION_TYPE;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.CHOICE_TYPE;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.ENTITY_TYPE_MAP;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.OWL_THING_TYPE;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.SPECIFIC_RESOURCE_TYPE;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.TEXT_POSITION_SELECTOR_TYPE;
import static ca.lincsproject.nssi.annotation.service.AnnotationConstants.XPATH_SELECTOR_TYPE;

@Profile(AnnotationModuleConstants.PROFILE_NAME)
@Service
public class AnnotationService {

    private final AnnotationClient annotationClient;
    private final AnnotationContext annotationContext;
    private final AnnotationCreator annotationCreator;

    private final Logger LOGGER = LoggerFactory.getLogger(AnnotationService.class);

    @Autowired
    public AnnotationService(AnnotationContext annotationContext,
                             AnnotationCreator annotationCreator,
                             AnnotationClient annotationClient) {
        this.annotationContext = annotationContext;
        this.annotationCreator = annotationCreator;
        this.annotationClient = annotationClient;
    }


    public Annotation createAnnotationForLinkedEntity(String documentURI,
                                                      DocumentMIMEType format,
                                                      LinkedEntityInfo info,
                                                      Selection selection) {
        Annotation annotation = Annotation.builder()
                .jsonContext(annotationContext)
                .type(ANNOTATION_TYPE)
                .creator(annotationCreator)
                .created(LocalDateTime.now())
                .issued(LocalDateTime.now())
                .motivatedBy("oa:identifying")
                .target(getAnnotationTarget(documentURI,
                        format.getMediaTypeName(),
                        selection))
                .body(AnnotationChoice.builder()
                        .type(CHOICE_TYPE)
                        .items(getAnnotationBodies(
                                info.getLinks(),
                                info.getNamedEntityInfo().getClassification()))
                        .build())
                .build();

        return annotation;
    }

    public void createAnnotationsForLinkedEntity(Long jobId,
                                                 String messageId,
                                                 String documentURI,
                                                 String resultsURI,
                                                 DocumentMIMEType format,
                                                 LinkedEntityInfo linkedEntityInfo,
                                                 String requestId) {
        for (NamedEntityInfo.SelectionWithLemma selection : linkedEntityInfo.getNamedEntityInfo().getSelections()) {
            Annotation annotation = createAnnotationForLinkedEntity(
                    documentURI, format, linkedEntityInfo, selection.getSelection()
            );

            annotationClient.addAnnotation(
                    resultsURI,
                    annotation,
                    requestId
            );
        }
    }

    public AnnotationContainer getAnnotationContainer(String containerId) {
        return AnnotationContainer.builder().build(); // massive TODO
    }

    private AnnotationTarget getAnnotationTarget(String documentURI,
                                                 String format,
                                                 Selection selection) {
        XpathSelector.Builder xpathSelectorBuilder = XpathSelector.builder()
                .type(XPATH_SELECTOR_TYPE)
                .value(selection.getXpath());

        if (Objects.nonNull(selection.getStart()) && Objects.nonNull(selection.getEnd())) {
            xpathSelectorBuilder.refinedBy(
                    TextPositionSelector.builder()
                            .type(TEXT_POSITION_SELECTOR_TYPE)
                            .start(selection.getStart())
                            .end(selection.getEnd())
                            .build()
            );
        }

        return AnnotationTarget.builder()
                .type(SPECIFIC_RESOURCE_TYPE)
                .source(documentURI)
                .format(format)
                .selector(xpathSelectorBuilder.build())
                .build();
    }

    private List<AnnotationBody> getAnnotationBodies(List<LinkingServiceResult> links,
                                                     EntityClassification classification) {
        String type = Optional.ofNullable(classification)
                .map(c -> ENTITY_TYPE_MAP.getOrDefault(c, OWL_THING_TYPE))
                .orElse(OWL_THING_TYPE);

        if (Objects.isNull(links)) {
            return Collections.emptyList();
        }

        return links.stream()
                .map(LinkingServiceResult::getMatches)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .map(linkDetails -> AnnotationBody.builder()
                        .type(type)
                        .id(linkDetails.getUri())
                        .format("text/plain")
                        .label(linkDetails.getHeading())
                        .description(linkDetails.getDescription())
                        .build())
                .collect(Collectors.toList());
    }
}
