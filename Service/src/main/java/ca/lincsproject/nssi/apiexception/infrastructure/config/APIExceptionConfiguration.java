package ca.lincsproject.nssi.apiexception.infrastructure.config;

import ca.lincsproject.nssi.apiexception.APIExceptionModuleConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile(APIExceptionModuleConstants.PROFILE_NAME)
@Configuration
public class APIExceptionConfiguration {

}
