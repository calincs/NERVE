package ca.lincsproject.nssi.standoff;

import ca.lincsproject.nssi.standoff.model.Annotation;
import ca.lincsproject.nssi.standoff.service.StandoffConverter;
import ca.lincsproject.nssi.standoff.service.StandoffDocument;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StandoffTest {

    @Test
    public void convert() throws ParserConfigurationException, IOException, SAXException {
        StandoffConverter converter = new StandoffConverter();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(StandoffConverter.class.getClassLoader().getResource("narrative.xml").getFile());

        StandoffDocument d = converter.toStandoff(doc);

        List<Annotation> annotations = d.getAnnotations();
        assertEquals(annotations.size(), 30);

        List<String> names = Arrays.asList("Henry", "Norwey", "Sweveland", "Fynmarke", "king of Denmarke", "Norway ",
                "Sweveland", "Realme of England", "Norwey", "Iles of Fynmarke", "Englishmen", "Realme of Norwey",
                "Marchandises", "Towne of Northberne", "king of Denmarke", "Englishmen", "Marchandises", "Englishmen",
                "Hans ", "Realme of England", "Realme of England", "king of Denmarke");

        for (Annotation ann : annotations) {
            if (ann.getLabel().equals("name")) {
                String name = d.getPlain().substring(ann.getStart(), ann.getEnd());
                assertTrue(names.contains(name));
            }
        }
    }
}
