{{/*
Defines the pod metadata for a NSSI service.
*/}}
{{- define "nssi.service.pod.metadata" -}}
    metadata:
      annotations:
        checksum/application-secrets: "{{ .root.Values.application.secretChecksum }}"
        {{ if .root.Values.gitlab.app }}app.gitlab.com/app: {{ .root.Values.gitlab.app | quote }}{{ end }}
        {{ if .root.Values.gitlab.env }}app.gitlab.com/env: {{ .root.Values.gitlab.env | quote }}{{ end }}
        prometheus.io/scrape: 'true'
        prometheus.io/port: '8080'
        prometheus.io/path: '/actuator/prometheus'
      labels:
        app: nssi
        component: "{{ .serviceData.name }}-{{ .serviceData.tierName }}"
{{- end -}}

{{/*
Defines the pod spec for a NSSI service.
*/}}
{{- define "nssi.service.pod.spec" -}}
    spec:
      imagePullSecrets:
{{ toYaml .root.Values.image.secrets | indent 10 }}
      initContainers:
      - name: wait-for-db
        image: postgres:11
        command: ["/bin/sh", "-c", "until pg_isready -h nssidb -p 5432; do echo waiting for nssidb; sleep 2; done;"]
      priorityClassName: {{ .serviceData.priorityClassName }}
      containers:
      - name: "{{ .serviceData.name }}-{{ .serviceData.tierName }}"
        image: {{ template "imagename" .root }}
        imagePullPolicy: {{ .root.Values.image.pullPolicy }}
{{- if .root.Values.application.secretName }}
        envFrom:
        - secretRef:
            name: {{ .root.Values.application.secretName }}
{{- end }}
        ports:
        - containerPort: 8080
        env:
        - name: GITLAB_ENVIRONMENT_NAME
          value: {{ .root.Values.gitlab.envName }}
        - name: GITLAB_ENVIRONMENT_URL
          value: {{ .root.Values.gitlab.envURL }}
        - name: SPRING_PROFILES_ACTIVE
          value: $(ENV_PROFILE_NAME),{{ .serviceData.profiles }}
        livenessProbe:
          httpGet:
            path: "/actuator/health"
            scheme: "HTTP"
            port: 8080
          initialDelaySeconds: 60
          timeoutSeconds: 300
        readinessProbe:
          httpGet:
            path: "/actuator/health"
            scheme: "HTTP"
            port: 8080
          initialDelaySeconds: 60
          timeoutSeconds: 300
        resources:
{{ toYaml .root.Values.resources | indent 12 }}
{{- end -}}

{{/*
Defines the deployment for a NSSI service.
*/}}
{{- define "nssi.service.deployment" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "{{ .serviceData.name }}-{{ .serviceData.tierName }}"
  annotations:
    {{ if .root.Values.gitlab.app }}app.gitlab.com/app: {{ .root.Values.gitlab.app | quote }}{{ end }}
    {{ if .root.Values.gitlab.env }}app.gitlab.com/env: {{ .root.Values.gitlab.env | quote }}{{ end }}
  labels:
    app: nssi
spec:
  replicas: {{ .root.Values.deploymentConfig.replicaCount }}
{{- if .root.Values.deploymentConfig.strategyType }}
  strategy:
    type: {{ .root.Values.deploymentConfig.strategyType | quote }}
{{- end }}
  selector:
    matchLabels:
      app: nssi
      component: "{{ .serviceData.name }}-{{ .serviceData.tierName }}"
  template:
    {{ include "nssi.service.pod.metadata" (dict "root" .root "serviceData" .serviceData) }}
    {{ include "nssi.service.pod.spec" (dict "root" .root "serviceData" .serviceData) }}
{{- end -}}

{{/*
Defines a Service object for the API microservice in each NSSI tier
*/}}
{{- define "nssi.api.service" -}}
apiVersion: v1
kind: Service
metadata:
{{- if .root.Values.prometheus.metrics }}
  prometheus.io/scrape: "true"
  prometheus.io/port: 8080
{{- end }}
  name: "{{ .serviceData.name}}-{{ .serviceData.tierName }}"
  labels:
    app: nssi
spec:
  ports:
  - port: 8080
  selector:
    app: nssi
    component: "{{ .serviceData.name}}-{{ .serviceData.tierName }}"
  type: ClusterIP
{{- end }}
