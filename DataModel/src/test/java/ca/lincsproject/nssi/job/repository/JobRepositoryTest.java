package ca.lincsproject.nssi.job.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JobRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private JobRepository jobRepository;

    @Test
    public void updateStatuses() {
        Job job1 = basicJob();
        testEntityManager.persistAndFlush(job1);
        assertFalse(job1.getAllCompleted());

        jobRepository.markCompleted(job1.getId());
        Optional<Job> completedJob = jobRepository.findById(job1.getId());
        assertTrue(completedJob.isPresent());
        assertTrue(completedJob.get().getAllCompleted());

        Job job2 = basicJob();
        testEntityManager.persistAndFlush(job2);
        assertFalse(job2.getCancelled());

        jobRepository.markCancelled(job2.getId());
        Optional<Job> cancelledJob = jobRepository.findById(job2.getId());
        assertTrue(cancelledJob.isPresent());
        assertTrue(cancelledJob.get().getCancelled());

        Job job3 = basicJob();
        testEntityManager.persistAndFlush(job3);
        assertFalse(job3.getFailed());

        jobRepository.markFailed(job3.getId());
        Optional<Job> failedJob = jobRepository.findById(job3.getId());
        assertTrue(failedJob.isPresent());
        assertTrue(failedJob.get().getFailed());
    }

    private Job basicJob() {
        return Job.builder()
                .createdBy("test")
                .startTime(LocalDateTime.now())
                .lastUpdatedTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .build();
    }
}
