package ca.lincsproject.nssi.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "workflowETA")
@IdClass(WorkflowETAId.class)
public class WorkflowETA {
    @Id
    private String workflow;
    @Id
    private String tierName;
    private Double eta;
}