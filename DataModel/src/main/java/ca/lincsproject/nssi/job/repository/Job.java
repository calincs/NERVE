package ca.lincsproject.nssi.job.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "jobs")
public class Job {
    /**
     * The job ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The JSON context sent with the job submission request.
     */
    @Column(columnDefinition = "TEXT")
    private String context;

    /**
     * The workflow under which the job is processed.
     */
    private String workflow;

    /**
     * The URI where job results can be obtained from.
     */
    private String resultsUri;

    /**
     * The user ID of the user who created the job.
     */
    private String createdBy;

    /**
     * The ID of the client subscribed to notifications for this job.
     */
    private String clientId;

    /**
     * The request ID for the job. FIXME: Does this need to be here?
     */
    private String requestId;

    /**
     * The start time of the job.
     */
    private LocalDateTime startTime;

    /**
     * The last updated time of the job.
     */
    private LocalDateTime lastUpdatedTime;

    /**
     * The total tasks that make up the job. For example, if a document contains
     * 40 entities to be linked and converted to annotations, since each of those
     * entities is processed separately, the `numTotal` for that job might be 81,
     * given by:     1 (running NER on the document to generate a list of 40 entities)
     *            + 40 (linking each of the 40 entities)
     *            + 40 (generating an annotation for each of the 40 entities)
     */
    private Integer numTotal;

    /**
     * The number of tasks completed for the job.
     */
    private Integer numCompleted;

    /**
     * The number of transient failures triggered by the job. This is when an error
     * occurs during processing but, on retrying, the processing succeeds.
     */
    private Integer numTransientFailures;

    /**
     * The number of permanent failures triggered by the job. This is when an error
     * persists through the maximum number of retries.
     */
    private Integer numPermanentFailures;

    /**
     * Whether all tasks for the job have been completed.
     */
    private Boolean allCompleted;

    /**
     * Whether the job has been cancelled.
     */
    private Boolean cancelled;

    /**
     * Whether the job has failed.
     */
    private Boolean failed;

    /**
     * The results for this job.
     */
    @OneToMany(mappedBy = "job")
    private List<Result> results;
}
