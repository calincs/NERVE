package ca.lincsproject.nssi.job.service;

import ca.lincsproject.nssi.job.repository.WorkflowETA;
import ca.lincsproject.nssi.job.repository.WorkflowETARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Hashtable;
import java.util.List;

@Service
@Transactional
public class WorkflowETAService {
    private final WorkflowETARepository workflowETARepo;
    private final String tierName;

    private final Hashtable<String, LocalDateTime> elapsedTime = new Hashtable<>();
    private final Hashtable<String, Boolean> activeTime = new Hashtable<>();

    @Autowired
    public WorkflowETAService(WorkflowETARepository workflowETARepo,
                              @Value("${nssi.tier_name:default}") String tierName)
    {
        this.workflowETARepo = workflowETARepo;
        this.tierName = tierName;
    }

    public Double getWorkflowETA(String workflow) {
        List<WorkflowETA> result = workflowETARepo.findByWorkflowAndTierName(workflow, tierName);
        if (result.isEmpty()) {
            return 0.0;
        }

        return result.get(0).getEta();
    }

    public void updateWorkflowETA(String workflow, Double estimated) {
        WorkflowETA workflowETA = WorkflowETA.builder()
                .workflow(workflow)
                .eta(estimated)
                .tierName(tierName)
                .build();

        workflowETARepo.save(workflowETA);
    }
}