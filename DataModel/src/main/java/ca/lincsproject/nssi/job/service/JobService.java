package ca.lincsproject.nssi.job.service;

import ca.lincsproject.nssi.job.repository.Job;
import ca.lincsproject.nssi.job.repository.JobRepository;
import ca.lincsproject.nssi.job.repository.Result;
import ca.lincsproject.nssi.job.repository.ResultRepository;
import ca.lincsproject.nssi.job.JobModuleConstants;
import ca.lincsproject.nssi.job.model.JobDTO;
import ca.lincsproject.nssi.job.model.JobStatus;
import ca.lincsproject.nssi.job.model.ResultDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Service
@Transactional
public class JobService {
    private final JobRepository jobRepository;
    private final ResultRepository resultRepository;
    private final ObjectMapper objectMapper;

    private final TypeReference<HashMap<String, String>> MAP_TYPEREF = new TypeReference<>() {
    };

    private final Logger LOGGER = LoggerFactory.getLogger(JobService.class);

    @Autowired
    JobService(JobRepository jobRepository,
               ResultRepository resultRepository,
               ObjectMapper objectMapper)
    {
        this.jobRepository = jobRepository;
        this.resultRepository = resultRepository;
        this.objectMapper = objectMapper;
    }

    public Long newJob(String user, Map<String, String> context, String workflow, String clientId, String requestId) {
        String contextJson = null;
        try {
            contextJson = objectMapper.writeValueAsString(context);
        } catch (JsonProcessingException e) {
            LOGGER.error("FAILED To Create New Job",
                    kv("stackTrace", e.getStackTrace()),
                    kv("service", JobModuleConstants.SERVICE_NAME),
                    kv("client", user),
                    kv("requestId", requestId));
        }

        Job job = Job.builder()
                .createdBy(user)
                .clientId(clientId)
                .requestId(requestId)
                .context(contextJson)
                .workflow(workflow)
                .startTime(LocalDateTime.now())
                .numTotal(0)
                .numCompleted(0)
                .numTransientFailures(0)
                .numPermanentFailures(0)
                .allCompleted(false)
                .cancelled(false)
                .failed(false)
                .lastUpdatedTime(LocalDateTime.now())
                .build();

        jobRepository.save(job);

        return job.getId();
    }

    public Optional<JobDTO> getJob(Long jobId) {
        return jobRepository.findById(jobId)
                .map(job -> toJobDTO(job, job.getRequestId()));
    }

    public List<JobDTO> getUncompletedJobs() {
        return jobRepository.findByAllCompletedFalse().stream()
                .map(job -> toJobDTO(job, job.getRequestId()))
                .collect(Collectors.toList());
    }

    public void updateResultsUri(Long jobId, String resultsUri) {
        jobRepository.findById(jobId)
                .ifPresent(job -> {
                    job.setResultsUri(resultsUri);
                    jobRepository.save(job);
                });
    }

    public void updateCompletedStatus(Long jobId) {
        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                    jobRepository.markCompleted(jobId);
                });
    }

    public void updateFailedStatus(Long jobId) {
        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                        jobRepository.markFailed(job.getId());
                });
    }

    public void updateResultsInfo(Long jobId, String resultStoreParams) {

        jobRepository.findById(jobId)
                .filter(job -> nullOrFalse(job.getCancelled()) && nullOrFalse(job.getFailed()))
                .ifPresent(job -> {
                    Result result = Result.builder()
                            .job(job)
                            .isOnS3(true)
                            .resultStoreParams(resultStoreParams)
                            .build();
                    resultRepository.save(result);
                });
    }

    public void cancelJob(Long jobId) {
        jobRepository.markCancelled(jobId);
    }

    public void deleteJob(Long jobId) {
        jobRepository.findById(jobId).ifPresent(job -> {
            job.getResults().forEach(resultRepository::delete);
        });

        jobRepository.deleteById(jobId);
    }

    public void deleteResult(Long jobId, Long resultId) {
        resultRepository.findById(resultId)
                .filter(result -> Objects.equals(result.getJob().getId(), jobId))
                .ifPresent(result -> {
                    resultRepository.deleteById(resultId);
                });
    }

    private JobDTO toJobDTO(Job job, String requestId) {
        return JobDTO.builder()
                .id(job.getId())
                .workflow(job.getWorkflow())
                .resultsUri(job.getResultsUri())
                .createdBy(job.getCreatedBy())
                .clientId(job.getClientId())
                .startTime(job.getStartTime())
                .lastUpdatedTime(job.getLastUpdatedTime())
                .status(getStatus(job.getAllCompleted(), job.getCancelled(), job.getFailed()))
                .results(toResultDTOList(job, requestId))
                .build();
    }

    private List<ResultDTO> toResultDTOList(Job job, String requestId) {
        List<ResultDTO> results = new ArrayList<>();

        if (Objects.isNull(job.getResults())) {
            return results;
        }

        for (Result result : job.getResults()) {
            Map<String, String> resultStoreParams = Collections.emptyMap();
            try {
                if (!job.getResults().isEmpty()
                        && Objects.nonNull(job.getResults().get(0).getResultStoreParams())) {
                    resultStoreParams = objectMapper.readValue(
                            job.getResults().get(0).getResultStoreParams(), MAP_TYPEREF
                    );
                }

                ResultDTO resultDTO = ResultDTO.builder()
                        .id(result.getId())
                        .isOnS3(result.isOnS3())
                        .resultStoreParams(resultStoreParams)
                        .document(result.getDocument())
                        .build();

                results.add(resultDTO);
            } catch (Exception e) {
                LOGGER.error("FAILED To Create ResultDTO",
                        kv("stackTrace", e.getStackTrace()),
                        kv("service", JobModuleConstants.SERVICE_NAME),
                        kv("jobId", job.getId()),
                        kv("requestId", requestId));
            }
        }

        return results;
    }

    private JobStatus getStatus(boolean allCompleted,
                                boolean cancelled,
                                boolean failed)
    {
        if (cancelled) {
            return JobStatus.CANCELLED;
        }

        if (failed) {
            return JobStatus.FAILED;
        }

        if (allCompleted) {
            return JobStatus.READY;
        }

        return JobStatus.IN_PROGRESS;
    }

    private boolean nullOrFalse(Boolean b) {
        return b == null || !b;
    }
}
