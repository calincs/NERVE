package ca.lincsproject.nssi.job.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Data-transfer object for job result information.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultDTO {
    /**
     * The result ID.
     */
    private Long id;

    /**
     * Whether the results are saved on S3.
     */
    private boolean isOnS3;

    /**
     * A map containing parameters that that describe how to access results
     * from S3 (if applicable)
     */
    private Map<String, String> resultStoreParams;

    /**
     * A JSON string containing the results.
     */
    private String document;
}
