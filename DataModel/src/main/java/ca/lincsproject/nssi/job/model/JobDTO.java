package ca.lincsproject.nssi.job.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Data-transfer object for job information.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobDTO {
    /**
     * The job ID.
     */
    private Long id;

    /**
     * The request ID. [FIXME: this shouldn't need to be here.]
     */
    private String requestId;

    /**
     * The workflow that the job is processed under.
     */
    private String workflow;

    /**
     * The URI where job results can be obtained from.
     */
    private String resultsUri;

    /**
     * The user ID of the user who created the job.
     */
    private String createdBy;

    /**
     * The ID of the client subscribed to notifications for this job.
     */
    private String clientId;

    /**
     * The start time of the job.
     */
    private LocalDateTime startTime;

    /**
     * The last updated time of the job.
     */
    private LocalDateTime lastUpdatedTime;

    /**
     * The status of the job.
     */
    private JobStatus status;

    /**
     * The results for this job.
     */
    private List<ResultDTO> results;
}
