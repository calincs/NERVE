package ca.lincsproject.nssi.job.repository;

import ca.lincsproject.nssi.job.model.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TaskUpdateRepository extends JpaRepository<TaskUpdate, Long> {
    List<TaskUpdate> findByJobId(Long jobId);
    List<TaskUpdate> findByJobIdAndStatus(Long jobId, TaskStatus status);

    @Query("select distinct jobId from #{#entityName}")
    List<Long> findDistinctJobId();

    Integer countTaskUpdateByServiceNameEqualsAndStatusEqualsAndUpdateTimeBetween(
            String serviceName,
            TaskStatus status,
            LocalDateTime start,
            LocalDateTime end
    );

    Integer countTaskUpdateByServiceNameEqualsAndStatusEquals(
            String serviceName,
            TaskStatus status
    );

    void deleteByJobId(Long jobId);
}
