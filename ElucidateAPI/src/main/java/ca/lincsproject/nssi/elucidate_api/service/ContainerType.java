package ca.lincsproject.nssi.elucidate_api.service;


import com.fasterxml.jackson.annotation.JsonValue;

public enum ContainerType {
    BASIC_CONTAINER("BasicContainer"),
    ANNOTATION_COLLECTION("AnnotationCollection");

    private final String name;

    ContainerType(String name) {
        this.name = name;
    }

    @JsonValue
    String getName() {
        return this.name;
    }

}
